import { MouseEvent } from 'react'

export type ButtonEvent =
  | MouseEvent<HTMLButtonElement, MouseEvent>
  | MouseEvent<HTMLAnchorElement, MouseEvent>

export interface ButtonProps {
  to?: string
  onInteraction?: (event: ButtonEvent) => void
  href?: string
  appearance?: 'primary'
  text: string
}
