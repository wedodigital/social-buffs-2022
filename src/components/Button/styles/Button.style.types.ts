import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledButtonProps {
  theme: Theme;
}
