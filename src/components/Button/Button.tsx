import React, { PureComponent, ReactNode } from 'react'
import { Link } from 'gatsby'

import * as Styled from './styles/Button.style'

import { ButtonProps, ButtonEvent } from './Button.types'

export default class Button extends PureComponent<ButtonProps> {
  public static defaultProps: Partial<ButtonProps> = {
    appearance: 'primary',
  }

  private onInteraction = (event: ButtonEvent): void => {
    if (this.props.onInteraction) {
      this.props.onInteraction(event)
    }
  }

  private get buttonType() {
    if (this.props.to) {
      return Link
    }
    if (this.props.href) {
      return 'a'
    }
    return 'button'
  }

  render(): ReactNode {
    return (
      <Styled.Button
        as={this.buttonType}
        onClick={this.onInteraction}
        to={this.props.to}
        href={this.props.href}
        appearance={this.props.appearance}
      >
        {this.props.text}
      </Styled.Button>
    )
  }
}
