import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledLogoGridProps {
  theme: Theme;
}
