import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledLogoGridProps } from './LogoGrid.style.types'

export const LogoGrid = styled.div((props: StyledLogoGridProps): FlattenSimpleInterpolation => css`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  margin: 0 -${props.theme.spacing.fixed[3]}px -${props.theme.spacing.fixed[3]}px;
  padding: 0 ${props.theme.spacing.fixed[3]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin: 0 -${props.theme.spacing.fixed[4]}px;
    padding: 0;
  `)}
`)

export const LogoBlock = styled.div((props: StyledLogoGridProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${props.theme.spacing.fixed[3]}px;
  width: calc(100% / 3);
  flex-grow: 0;

  ${props.theme.mixins.respondTo.md(css`
    width: calc(100% / 6);
    padding: 0 ${props.theme.spacing.fixed[6]}px;
  `)}

  img {
    width: 100%;
  }
`)
