import React, { ReactElement, FC } from 'react'

import * as Styled from './styles/LogoGrid.style'

import { LogoGridProps } from './LogoGrid.types'

const LogoGrid: FC<LogoGridProps> = ({
  logos,
}: LogoGridProps): ReactElement => {
  return (
    <Styled.LogoGrid>
      {
        logos.map((logoImg, index) => {
          return (
            <Styled.LogoBlock key={index}>
              <img src={logoImg.logo?.sourceUrl} />
            </Styled.LogoBlock>
          )
        })
      }
    </Styled.LogoGrid>
  )
}

export default LogoGrid
