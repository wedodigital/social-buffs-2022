export interface SplitContentProps {
  image: string
  title: string
  content: string
  orientation: 'default' | 'reverse'
}
