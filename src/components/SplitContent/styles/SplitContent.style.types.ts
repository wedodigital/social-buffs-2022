import { Theme } from '@themes/sbTheme/sbTheme.types'

import { SplitContentProps } from '../SplitContent.types'

export interface StyledSplitContentProps {
  theme: Theme;
  backgroundImage: string
  orientation: SplitContentProps['orientation']
}
