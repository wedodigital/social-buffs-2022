import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledSplitContentProps } from './SplitContent.style.types'

type SplitContentProps = Pick<StyledSplitContentProps, 'orientation' | 'theme'>
export const SplitContent = styled.div((props: SplitContentProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  flex-direction: column-reverse;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;

    ${props.orientation === 'reverse' && css`
      flex-direction: row-reverse;
    `}
  `)}
`)

type ContentProps = Pick<StyledSplitContentProps, 'orientation' | 'theme'>
export const Content = styled.div((props: ContentProps): FlattenSimpleInterpolation => css`
  ${props.theme.mixins.respondTo.md(css`

    ${props.orientation === 'default' && css`
      padding-right: ${props.theme.spacing.fixed[8]}px;
    `}

    ${props.orientation === 'reverse' && css`
      padding-left: ${props.theme.spacing.fixed[8]}px;
    `}
  `)}
`)

type ImageProps = Pick<StyledSplitContentProps, 'theme' | 'backgroundImage'>
export const Image = styled.div((props: ImageProps): FlattenSimpleInterpolation => css`
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
  aspect-ratio: 1 / 1;
  width: 100%;
  flex-shrink: 0;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;

  ${props.theme.mixins.respondTo.md(css`
    width: 50%;
    margin-bottom: 0;
  `)}

  @supports not (aspect-ratio: 16 / 9) {
    &::before {
      float: left;
      padding-top: 100%;
      content: '';
    }
    &::after {
      display: block;
      content: '';
      clear: both;
    }
  }
`)
