import React, { ReactElement, FC } from 'react'

import SimpleContentBlock from '@components/SimpleContentBlock'

import * as Styled from './styles/SplitContent.style'

import { SplitContentProps } from './SplitContent.types'

const SplitContent: FC<SplitContentProps> = ({
  image,
  title,
  content,
  orientation = 'default',
}: SplitContentProps): ReactElement => {
  return (
    <Styled.SplitContent orientation={orientation}>
      <Styled.Content orientation={orientation}>
        <SimpleContentBlock title={title.toUpperCase()} content={content} />
      </Styled.Content>
      <Styled.Image backgroundImage={image} />
    </Styled.SplitContent>
  )
}

export default SplitContent
