import React, { ReactElement, FC } from 'react'

import * as Styled from './styles/Container.style'

import { ContainerProps } from './Container.types'

const Container: FC<ContainerProps> = ({
  children,
  width = 'default',
}: ContainerProps): ReactElement => {
  return (
    <Styled.Container width={width}>
      {children}
    </Styled.Container>
  )
}

export default Container
