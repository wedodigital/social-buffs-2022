import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledContainerProps } from './Container.style.types'

export const Container = styled.div((props: StyledContainerProps): FlattenSimpleInterpolation => css`
  width: 86%;
  margin: 0 auto;
  max-width: 1190px;

  ${props.width === 'wide' && css`
    width: 100%;
    max-width: calc(100% - ${props.theme.spacing.fixed[3] * 2}px);
  `}

  ${props.width === 'mid' && css`
    max-width: 1000px;
  `}
`)
