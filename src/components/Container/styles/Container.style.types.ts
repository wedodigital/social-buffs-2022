import { Theme } from '@themes/sbTheme/sbTheme.types'

import { ContainerProps } from '../Container.types'

export interface StyledContainerProps {
  theme: Theme;
  width: ContainerProps['width']
}
