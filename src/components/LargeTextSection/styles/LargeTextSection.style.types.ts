import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledLargeTextSectionProps {
  theme: Theme;
}
