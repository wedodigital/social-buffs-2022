import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { RawHtmlWrapper } from '@components/RawHtmlWrapper/styles/RawHtmlWrapper.style'

import { StyledLargeTextSectionProps } from './LargeTextSection.style.types'

export const LargeTextSection = styled.div((props: StyledLargeTextSectionProps): FlattenSimpleInterpolation => css`
  ${RawHtmlWrapper} {
    * {
      font-size: ${props.theme.typography.heading[3].fontSizeMobile};
      line-height: ${props.theme.typography.heading[3].lineHeightMobile};
      font-family: ${props.theme.typography.fontFamily.feature};

      ${props.theme.mixins.respondTo.md(css`
        font-size: ${props.theme.typography.heading[3].fontSize};
        line-height: ${props.theme.typography.heading[3].lineHeight};
      `)}

      &:first-child {
        color: ${props.theme.colours.gold};
      }
    }
  }
`)
