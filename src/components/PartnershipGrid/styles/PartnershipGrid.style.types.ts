import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledPartnershipGridProps {
  theme: Theme;
}
