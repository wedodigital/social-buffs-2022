import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledPartnershipGridProps } from './PartnershipGrid.style.types'

export const PartnershipGrid = styled.div((props: StyledPartnershipGridProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-wrap: wrap;
  margin: ${props.theme.spacing.fixed[4]}px -${props.theme.spacing.fixed[2]}px;
`)
