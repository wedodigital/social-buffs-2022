export interface PartnershipGridProps {
  title: string
  partners: {
    companyLogo: {
      sourceUrl: string
    }
  }[]
}
