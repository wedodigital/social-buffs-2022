import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'

import PartnerTile from './components/PartnerTile'

import * as Styled from './styles/PartnershipGrid.style'

import { PartnershipGridProps } from './PartnershipGrid.types'

const PartnershipGrid: FC<PartnershipGridProps> = ({
  title,
  partners,
}: PartnershipGridProps): ReactElement => {
  return (
    <>
      <Heading text={title} size={3} noMargin />
      <Styled.PartnershipGrid>
        {
          partners.map((partner, index) => {
            return (
              <PartnerTile {...partner} key={index} />
            )
          })
        }
      </Styled.PartnershipGrid>
    </>
  )
}

export default PartnershipGrid
