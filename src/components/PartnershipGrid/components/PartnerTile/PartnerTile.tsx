import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'

import * as Styled from './styles/PartnerTile.style'

import { PartnerTileProps } from './PartnerTile.types'

const PartnerTile: FC<PartnerTileProps> = ({
  companyLogo,
}: PartnerTileProps): ReactElement => {
  return (
    <Styled.PartnerTile>
      <div>
        <img src={companyLogo.sourceUrl} alt='Official Partner' />
        <Heading size={1} text='Official Partner' />
      </div>
    </Styled.PartnerTile>
  )
}

export default PartnerTile
