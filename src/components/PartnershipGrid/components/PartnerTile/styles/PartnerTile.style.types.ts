import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledPartnerTileProps {
  theme: Theme;
}
