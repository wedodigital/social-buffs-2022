import { ReactNode } from 'react'

export interface SectionProps {
  /**
   * React children
   * */
  children: ReactNode
  titleSection?: boolean
  paddingLevel?: 1 | 2
}
