import React, { ReactElement, FC } from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import Heading from '@components/Heading'
import Arrow from '@components/Arrow'

import TeamMember from './components/TeamMember'

import * as Styled from './styles/TeamSlider.style'

import { TeamSliderProps } from './TeamSlider.types'

const TeamSlider: FC<TeamSliderProps> = ({
  title,
  team,
}: TeamSliderProps): ReactElement => {
  const settings = {
    dots: false,
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ],
    nextArrow: <Arrow direction='next' />,
    prevArrow: <Arrow direction='prev' />,
  }
  return (
    <>
      <Heading text={title} size={3} noMargin />
      <Styled.TeamSlider>
        <Slider {...settings}>
          {
            team.map((teamMember, index) => {
              return (
                <Styled.Slide key={index}>
                  <TeamMember
                    name={teamMember.name}
                    role={teamMember.role}
                    picture={teamMember.picture.sourceUrl}
                  />
                </Styled.Slide>
              )
            })
          }
        </Slider>
      </Styled.TeamSlider>
    </>
  )
}

export default TeamSlider
