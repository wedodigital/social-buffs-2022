export interface TeamSliderProps {
  title: string
  team: {
    name: string
    role: string
    picture: {
      sourceUrl: string
    }
  }[]
}
