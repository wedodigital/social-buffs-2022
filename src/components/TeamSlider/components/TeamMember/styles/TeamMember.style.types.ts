import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledTeamMemberProps {
  theme: Theme;
  backgroundImage: string
}
