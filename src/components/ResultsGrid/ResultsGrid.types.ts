export interface ResultsGridProps {
  title: string
  results: {
    percentage: string
    title: string
    companyLogo: {
      sourceUrl: string
    }
  }[]
}
