import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'

import * as Styled from './styles/ResultsGrid.style'

import { ResultsGridProps } from './ResultsGrid.types'

const ResultsGrid: FC<ResultsGridProps> = ({
  title,
  results,
}: ResultsGridProps): ReactElement => {
  return (
    <>
      <Heading text={title} size={3} noMargin />
      <Styled.ResultsGrid>
        {
          results.map((result, index) => {
            return (
              <Styled.Result key={index}>
                <Heading
                  size={4}
                  appearance='secondary'
                  text={`${result.percentage}`}
                />
                <Paragraph text={result.title} />
                <If condition={result.companyLogo?.sourceUrl}>
                  <Styled.CompanyLogo src={result.companyLogo.sourceUrl} alt={result.title} />
                </If>
              </Styled.Result>
            )
          })
        }
      </Styled.ResultsGrid>
    </>
  )
}

export default ResultsGrid
