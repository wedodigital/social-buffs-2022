import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledResultsGridProps {
  theme: Theme;
}
