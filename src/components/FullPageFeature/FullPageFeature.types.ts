export interface FullPageFeatureProps {
  title: string
  titleHighlight?: string
  titleSuffix?: string
  backgroundImage?: string
  tags?: {
    nodes: {
      name: string
    }[]
  }
  categories?: {
    nodes: {
      name: string
    }[]
  }
  backgroundVideo?: {
    poster: string
    mp4: string
    webm: string
  }
}
