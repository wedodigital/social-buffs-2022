import { HeadingSizeKeys } from '@themes/sbTheme/constants/typography.types'

export interface HeadingProps {
  text: string | string[]
  level?: 1 | 2 | 3 | 4 | 5
  size?: HeadingSizeKeys
  noMargin?: boolean
  inverse?: boolean
  weight?: 1 | 2 | 3
  highlightText?: string
  suffixText?: string
  appearance?: 'primary' | 'secondary'
  fontStyle?: 'feature' | 'body'
}
