import React, { FC, ReactElement } from 'react'

import { HeadingProps } from './Heading.types'

import * as Styled from './styles/Heading.style'

const Heading: FC<HeadingProps> = ({
  text,
  level = 2,
  size = 2,
  noMargin = false,
  inverse = false,
  weight = 1,
  highlightText,
  suffixText,
  appearance = 'primary',
  fontStyle = 'feature',
}: HeadingProps): ReactElement => {
  return (
    <Styled.Heading
      as={`h${level}` as React.ElementType}
      size={size}
      noMargin={noMargin}
      inverse={inverse}
      appearance={appearance}
      weight={weight}
      fontStyle={fontStyle}
    >
      <Choose>
        <When condition={highlightText && suffixText}>
          {text} <Styled.HighlightText>{highlightText}</Styled.HighlightText> {suffixText}
        </When>
        <When condition={highlightText}>
          <Styled.HighlightText>{highlightText}</Styled.HighlightText> {text}
        </When>
        <Otherwise>
          {text}
        </Otherwise>
      </Choose>
    </Styled.Heading>
  )
}

export default Heading
