import React, { ReactElement, FC } from 'react'
import Headroom from 'react-headroom'

import Logo from '@components/Logo'

import * as Styled from './styles/Header.style'

const Header: FC = (): ReactElement => {
  return (
    <Styled.Header>
      <Headroom>
        <Logo />
      </Headroom>
    </Styled.Header>
  )
}

export default Header
