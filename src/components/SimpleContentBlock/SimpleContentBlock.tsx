import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import * as Styled from './styles/SimpleContentBlock.style'

import { SimpleContentBlockProps } from './SimpleContentBlock.types'

const SimpleContentBlock: FC<SimpleContentBlockProps> = ({
  title,
  content,
}: SimpleContentBlockProps): ReactElement => {
  return (
    <Styled.SimpleContentBlock>
      <Heading level={2} size={1} text={title} />
      <RawHtmlWrapper content={content} />
    </Styled.SimpleContentBlock>
  )
}

export default SimpleContentBlock
