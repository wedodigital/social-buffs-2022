export interface SimpleContentBlockProps {
  title: string
  content: string
}
