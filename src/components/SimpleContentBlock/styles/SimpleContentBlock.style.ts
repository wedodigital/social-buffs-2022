import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledSimpleContentBlockProps } from './SimpleContentBlock.style.types'

export const SimpleContentBlock = styled.div((props: StyledSimpleContentBlockProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.grey};
`)
