import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledSimpleContentBlockProps {
  theme: Theme;
}
