import { Theme } from '@themes/sbTheme/sbTheme.types'

import { ParagraphProps } from '../Paragraph.types'

export interface StyledParagraphProps {
  theme: Theme
  size: ParagraphProps['size']
  weight: ParagraphProps['weight']
  inverse: boolean
}
