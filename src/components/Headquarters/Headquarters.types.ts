export interface HeadquartersProps {
  title: string
  content: string
  address: string
  email: string
  telephoneNumber: string
  image: string
}
