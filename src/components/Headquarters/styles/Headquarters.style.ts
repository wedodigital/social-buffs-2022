import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeadquartersProps } from './Headquarters.style.types'

export const Headquarters = styled.div((props: StyledHeadquartersProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-direction: column-reverse;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
    align-items: flex-end;
  `)}
`)

export const HeadquartersContent = styled.div((props: StyledHeadquartersProps): FlattenSimpleInterpolation => css`
  ${props.theme.mixins.respondTo.md(css`
    width: calc(34% - ${props.theme.spacing.fixed[6]}px);
    margin-right: ${props.theme.spacing.fixed[6]}px;
  `)}
`)

export const HeadQuartersImage = styled.div((props: StyledHeadquartersProps): FlattenSimpleInterpolation => css`
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
  aspect-ratio: 16 / 9;
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
  width: 100%;
  margin: ${props.theme.spacing.fixed[2]}px 0 ${props.theme.spacing.fixed[4]}px;

  ${props.theme.mixins.respondTo.md(css`
    width: 66%;
    margin: 0;
  `)}

  @supports not (aspect-ratio: 16 / 9) {
    &::before {
      float: left;
      padding-top: 56.25%;
      content: '';
    }
    &::after {
      display: block;
      content: '';
      clear: both;
    }
  }
`)

export const ContentBlock = styled.div((props: StyledHeadquartersProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[4]}px;

  ${props.altAppearance && css`
    color: ${props.theme.colours.grey};
  `}

  [class*=Link] {
    display: block;
    color: ${props.theme.colours.gold};
  }
`)
