import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledHeadquartersProps {
  theme: Theme;
  backgroundImage: string
  altAppearance: boolean
}
