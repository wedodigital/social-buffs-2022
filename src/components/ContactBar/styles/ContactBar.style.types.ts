import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledContactBarProps {
  theme: Theme
  appearance: 'primary' | 'secondary'
}
