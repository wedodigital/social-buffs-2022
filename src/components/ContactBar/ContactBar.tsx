import React, { ReactElement, FC } from 'react'

import Container from '@components/Container'
import Button from '@components/Button'
// import Link from '@components/Link'
import MatchMedia from '@components/MatchMedia'

import * as Styled from './styles/ContactBar.style'

const ContactBar: FC = (): ReactElement => {
  return (
    <Styled.ContactBar>
      <MatchMedia breakpoint='sm'>
        {
          // <Styled.MobileButton appearance='secondary' href='tel:+44(0)2032398402'>+44 (0) 203 239 8402</Styled.MobileButton>
        }
        <Styled.MobileButton appearance='primary' href='https://socialbuff.typeform.com/workwithus'>Work with us</Styled.MobileButton>
      </MatchMedia>
      <MatchMedia breakpoint='md' andAbove>
        <Container width='wide'>
          {
            // <Link href='tel:+44(0)2032398402' text='+44 (0) 203 239 8402' fontStyle='feature' />
          }
          <Button href='https://socialbuff.typeform.com/workwithus' text='Work with us' />
        </Container>
      </MatchMedia>
    </Styled.ContactBar>
  )
}

export default ContactBar
