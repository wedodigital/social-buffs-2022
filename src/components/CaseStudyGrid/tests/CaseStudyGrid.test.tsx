// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
/* eslint-disable react/react-in-jsx-scope */

/** *************************************************************************** */
/** Import - helpers                                                            */
/** --------------------------------------------------------------------------- */
/** Imports that are not being mocked or tested                                 */
/** *************************************************************************** */

/** *************************************************************************** */
/** Import - tested files                                                       */
/** --------------------------------------------------------------------------- */
/** Imports that unit tests will be written against                             */
/** *************************************************************************** */
import CaseStudyGrid from '../CaseStudyGrid'

/** *************************************************************************** */
/** Import - mocked files                                                       */
/** --------------------------------------------------------------------------- */
/** Imports that are defined only to be mocked eg stores, utils. helpers        */
/** *************************************************************************** */

/** *************************************************************************** */
/** Jest mocks                                                                  */
/** --------------------------------------------------------------------------- */
/** Globally defined jest mocks                                                 */
/** *************************************************************************** */

/** *************************************************************************** */
/** Unit testing                                                                */
/** *************************************************************************** */

const minProps = {
  caseStudies: [
    {
      node: {
        id: 1,
      }
    },
    {
      node: {
        id: 1,
      }
    }
  ]
}

describe('<CaseStudyGrid />', () => {
  describe('render', () => {
    it('should render in a grid', () => {
      const wrapper = shallow(<CaseStudyGrid {...minProps} />)
      expect(wrapper.exists()).toBe(true)
    })
    it('should render in a carousel', () => {
      const wrapper = shallow(<CaseStudyGrid {...minProps} format='carousel' />)
      expect(wrapper.exists()).toBe(true)
    })
  })
})
