import React, { ReactElement, FC } from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import Heading from '@components/Heading'
import Link from '@components/Link'
import Arrow from '@components/Arrow'
import MatchMedia from '@components/MatchMedia'

import CaseStudy from './components/CaseStudy'

import * as Styled from './styles/CaseStudyGrid.style'

import { CaseStudyGridProps } from './CaseStudyGrid.types'

const CaseStudyGrid: FC<CaseStudyGridProps> = ({
  title,
  caseStudies,
  showLink,
  format = 'grid',
}: CaseStudyGridProps): ReactElement => {
  const settings = {
    dots: false,
    infinite: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ],
    nextArrow: <Arrow direction='next' />,
    prevArrow: <Arrow direction='prev' />,
  }
  return (
    <>
      <If condition={title}>
        <Styled.TitleWrapper>
          <Heading text={title} size={3} noMargin />
          <If condition={showLink}>
            <Link to='/case-studies' text='View All' />
          </If>
        </Styled.TitleWrapper>
      </If>
      <Choose>
        <When condition={format === 'grid'}>
          <Styled.CaseStudyGrid>
            {
              caseStudies.map((caseStudyNode) => {
                const caseStudy = caseStudyNode.node
                return (
                  <CaseStudy format={format} {...caseStudy} key={caseStudyNode.id} />
                )
              })
            }
          </Styled.CaseStudyGrid>
        </When>
        <When condition={format === 'carousel'}>
          <MatchMedia breakpoint='sm'>
            <Styled.CaseStudyGrid>
              {
                caseStudies.map((caseStudyNode) => {
                  const caseStudy = caseStudyNode.node
                  return (
                    <CaseStudy format={format} {...caseStudy} key={caseStudyNode.id} />
                  )
                })
              }
            </Styled.CaseStudyGrid>
          </MatchMedia>
          <MatchMedia breakpoint='md' andAbove>
            <Styled.CaseStudySlider>
              <Slider {...settings}>
                {
                  caseStudies.map((caseStudyNode) => {
                    const caseStudy = caseStudyNode.node
                    return (
                      <Styled.Slide key={caseStudyNode.id}>
                        <CaseStudy format={format} {...caseStudy} key={caseStudyNode.id} />
                      </Styled.Slide>
                    )
                  })
                }
              </Slider>
            </Styled.CaseStudySlider>
          </MatchMedia>
        </When>
      </Choose>
    </>
  )
}

export default CaseStudyGrid
