import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'

import tagsList from '@helpers/tagsList'

import * as Styled from './styles/CaseStudy.style'

import { CaseStudyProps } from './CaseStudy.types'

const CaseStudy: FC<CaseStudyProps> = ({
  title,
  uri,
  categories,
  featuredImage,
  format,
}: CaseStudyProps): ReactElement => {
  return (
    <Styled.CaseStudy to={uri} backgroundImage={featuredImage?.node.sourceUrl} format={format}>
      <Styled.CaseStudyContent>
        <Heading size={2} text={title} noMargin />
        <Heading size={1} text={tagsList(categories)} fontStyle='body' />
      </Styled.CaseStudyContent>
    </Styled.CaseStudy>
  )
}

export default CaseStudy
