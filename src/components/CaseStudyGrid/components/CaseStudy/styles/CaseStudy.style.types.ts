import { Theme } from '@themes/sbTheme/sbTheme.types'

import { CaseStudyProps } from '../CaseStudy.types'

export interface StyledCaseStudyProps {
  theme: Theme
  backgroundImage: string
  format: CaseStudyProps['format']
}
