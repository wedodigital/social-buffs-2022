import styled, { css, FlattenSimpleInterpolation } from 'styled-components'
import { Link } from 'gatsby'

import { StyledCaseStudyProps } from './CaseStudy.style.types'

export const CaseStudy = styled(Link)((props: StyledCaseStudyProps): FlattenSimpleInterpolation => css`
  width: 100%;
  margin: 0 0 ${props.theme.spacing.fixed[2]}px;
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
  aspect-ratio: 16 / 9;
  position: relative;
  overflow: hidden;
  background: url(${props.backgroundImage}) center center no-repeat;
  background-size: cover;
  display: block;

  @supports not (aspect-ratio: 16 / 9) {
    &::before {
      float: left;
      padding-top: 56.25%;
      content: '';
    }
    &::after {
      display: block;
      content: '';
      clear: both;
    }
  }

  ${props.theme.mixins.respondTo.md(css`
    width: calc(50% - ${props.theme.spacing.fixed[3]}px);
    margin: ${props.theme.spacing.fixed[3] / 2}px;

    ${props.format === 'carousel' && css`
      width: 100%;
      margin: 0;
    `}
  `)}

  &::after {
    transition: 0.4s all ease;
    opacity: 0;
    content: '';
    display: block;
    position: absolute;
    top: 50%;
    left: 0;
    bottom: 0;
    right: 0;
    background: linear-gradient(to bottom, rgba(19, 20, 21, 0) 0%, rgba(19, 20, 21, .75) 100%);
  }

  &:hover {
    &::after {
      opacity: 1;
    }
  }
`)

export const CaseStudyContent = styled.div((props: StyledCaseStudyProps): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  align-items: flex-start;
  justify-content: flex-end;
  flex-direction: column;
  padding: ${props.theme.spacing.fixed[2]}px;
  color: ${props.theme.colours.white};
  z-index: 10;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[6]}px;
  `)}
`)
