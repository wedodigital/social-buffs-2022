import { CaseStudyGridProps } from '../CaseStudy.types'

export interface CaseStudyProps {
  title: string
  uri: string
  tags: {
    nodes: {
      name: string
    }[]
  }
  categories: {
    nodes: {
      name: string
    }[]
  }
  featuredImage: {
    node: {
      sourceUrl: string
    }
  }
  format: CaseStudyGridProps['format']
}
