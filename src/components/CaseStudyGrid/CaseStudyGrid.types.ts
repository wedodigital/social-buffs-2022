import { CaseStudyProps } from './components/CaseStudy/CaseStudy.types'

export interface CaseStudyGridProps {
  title?: string
  showLink?: boolean
  caseStudies: {
    id: string
    node: CaseStudyProps
  }[]
  format?: 'grid' | 'carousel'
}
