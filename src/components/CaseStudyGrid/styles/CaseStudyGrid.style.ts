import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledCaseStudyGridProps } from './CaseStudyGrid.style.types'

export const CaseStudyGrid = styled.div((props: StyledCaseStudyGridProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-wrap: wrap;

  ${props.theme.mixins.respondTo.md(css`
    margin: -${props.theme.spacing.fixed[3] / 2}px;
  `)}
`)

export const TitleWrapper = styled.div((props: StyledCaseStudyGridProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;
`)

export const CaseStudySlider = styled.div((props: StyledCaseStudyGridProps): FlattenSimpleInterpolation => css`
  margin: ${props.theme.spacing.fixed[8]}px -${props.theme.spacing.fixed[2]}px 0;
  position: relative;
  width: 100%;

  .slick-list {
    overflow: visible;
  }

  ${props.theme.mixins.respondTo.md(css`
    width: unset;

    .slick-list {
      overflow: hidden;
    }
  `)}
`)

export const Slide = styled.div((props: StyledCaseStudyGridProps): FlattenSimpleInterpolation => css`
  padding: 0 ${props.theme.spacing.fixed[2]}px;
`)
