import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledCaseStudyGridProps {
  theme: Theme;
}
