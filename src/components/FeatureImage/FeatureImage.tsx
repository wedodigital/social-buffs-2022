import React, { ReactElement, FC } from 'react'

import * as Styled from './styles/FeatureImage.style'

import { FeatureImageProps } from './FeatureImage.types'

const FeatureImage: FC<FeatureImageProps> = ({
  image
}: FeatureImageProps): ReactElement => {
  return (
    <Styled.FeatureImage backgroundImage={image} />
  )
}

export default FeatureImage
