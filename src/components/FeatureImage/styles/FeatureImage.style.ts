import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFeatureImageProps } from './FeatureImage.style.types'

export const FeatureImage = styled.div((props: StyledFeatureImageProps): FlattenSimpleInterpolation => css`
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
  aspect-ratio: 16 / 9;

  @supports not (aspect-ratio: 16 / 9) {
    &::before {
      float: left;
      padding-top: 56.25%;
      content: '';
    }
    &::after {
      display: block;
      content: '';
      clear: both;
    }
  }
`)
