import React, { ReactElement, FC } from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import Arrow from '@components/Arrow'

import * as Styled from './styles/ImageCarousel.style'

import { ImageCarouselProps } from './ImageCarousel.types'

const ImageCarousel: FC<ImageCarouselProps> = ({
  children,
}: ImageCarouselProps): ReactElement => {
  const settings = {
    dots: false,
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ],
    nextArrow: <Arrow direction='next' />,
    prevArrow: <Arrow direction='prev' />,
  }
  return (
    <Styled.ImageCarousel>
      <Slider {...settings}>
        {
          React.Children.map(children, child => {
            if (child === null) return null
            return (
              <Styled.Slide>
                {child}
              </Styled.Slide>
            )
          })
        }
      </Slider>
    </Styled.ImageCarousel>
  )
}

export default ImageCarousel
