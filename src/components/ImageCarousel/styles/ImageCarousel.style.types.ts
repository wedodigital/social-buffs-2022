import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledImageCarouselProps {
  theme: Theme;
}
