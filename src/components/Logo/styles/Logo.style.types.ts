import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledLogoProps {
  theme: Theme;
}
