import styled, { css, FlattenSimpleInterpolation } from 'styled-components'
import { Link } from 'gatsby'

import { StyledLogoProps } from './Logo.style.types'

export const Logo = styled(Link)((props: StyledLogoProps): FlattenSimpleInterpolation => css`
  display: inline-block;
  height: ${props.theme.spacing.fixed[2]}px;

  ${props.theme.mixins.respondTo.md(css`
    height: ${props.theme.spacing.fixed[3]}px;
  `)}

  svg {
    height: 100%;

    * {
      fill: ${props.theme.colours.white};
    }
  }
`)
