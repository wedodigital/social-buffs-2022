export interface CaseStudyIntroProps {
  intro: string
  overview: string
  tags: {
    nodes: {
      name: string
    }[]
  }
  url?: string
}
