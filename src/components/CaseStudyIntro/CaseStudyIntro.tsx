import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'
import SimpleContentBlock from '@components/SimpleContentBlock'
import Link from '@components/Link'

import * as Styled from './styles/CaseStudyIntro.style'

import { CaseStudyIntroProps } from './CaseStudyIntro.types'

const CaseStudyIntro: FC<CaseStudyIntroProps> = ({
  intro,
  overview,
  tags,
  url,
}: CaseStudyIntroProps): ReactElement => {
  return (
    <>
      <Styled.Intro>
        <RawHtmlWrapper content={intro} />
      </Styled.Intro>
      <Styled.Columns>
        <Styled.Content>
          <SimpleContentBlock
            title='01. OVERVIEW'
            content={overview}
          />
        </Styled.Content>
        <Styled.Meta>
          <Styled.ContentBlock>
            <Heading text='SERVICES' size={1} />
            <ul>
              {
                tags.nodes.map((tag, index) => {
                  return (
                    <li key={index}>{tag.name}</li>
                  )
                })
              }
            </ul>
          </Styled.ContentBlock>
          <If condition={url}>
            <Styled.ContentBlock>
              <Heading text='VISIT SITE' size={1} />
              <Link href={url} text={url} appearance='secondary' />
            </Styled.ContentBlock>
          </If>
        </Styled.Meta>
      </Styled.Columns>
    </>
  )
}

export default CaseStudyIntro
