import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledServiceGridProps } from './ServiceGrid.style.types'

export const ServiceGrid = styled.div((props: StyledServiceGridProps): FlattenSimpleInterpolation => css`
  ${props.theme.mixins.respondTo.md(css`
    display: flex;
    align-items: flex-start;
    margin: 0 -${props.theme.spacing.fixed[2]}px;
  `)}
`)

export const Service = styled.div((props: StyledServiceGridProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[6]}px;
  color: ${props.theme.colours.grey};

  &:last-child {
    margin-bottom: 0;
  }

  ${props.theme.mixins.respondTo.md(css`
    margin: 0 ${props.theme.spacing.fixed[2]}px;
    width: calc(100% - ${props.theme.spacing.fixed[2] * 2}px);
  `)}

  ul {
    margin-left: 0;

    li {
      list-style: none;
    }
  }
`)

export const ServiceHeading = styled.div((props: StyledServiceGridProps): FlattenSimpleInterpolation => css`
  border-bottom: 1px solid ${props.theme.colours.gold};
  padding-bottom: ${props.theme.spacing.fixed[2]}px;
  margin-bottom: ${props.theme.spacing.fixed[2]}px;

  & > [class*=Heading] {
    max-width: 90%;
  }
`)
