import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledServiceGridProps {
  theme: Theme;
}
