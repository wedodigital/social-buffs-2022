export interface ServiceGridProps {
  services: {
    title: string
    content: string
  }[]
}
