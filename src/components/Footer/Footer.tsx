import React, { ReactElement, FC } from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Container from '@components/Container'
import ContactBar from '@components/ContactBar'
import Link from '@components/Link'
import RawHtmlWrapper from '@components/RawHtmlWrapper'
import Heading from '@components/Heading'
import Logo from '@components/Logo'
import MatchMedia from '@components/MatchMedia'

import * as Styled from './styles/Footer.style'

const Footer: FC = (): ReactElement => {
  const footerContent = useStaticQuery(graphql`
    query footer {
      wpPage(databaseId: {eq: 375}) {
        footerContent {
          companyNumber
          vatNumber
          contactDetails {
            content
            title
          }
          privacyPolicy {
            localFile {
              publicURL
            }
          }
          socials {
            title
            url
          }
        }
      }
    }
  `)

  return (
    <>
      <ContactBar />
      <Styled.Footer>
        <Container>
          <Styled.Contact>
            <MatchMedia breakpoint='md' andAbove>
              <Styled.CompanyInfo>
                <Logo />
                <RawHtmlWrapper content={`<p>Social Buff Limited.<br />Company No: ${footerContent.wpPage.footerContent.companyNumber}  |  VAT No: ${footerContent.wpPage.footerContent.vatNumber}</p>`} />
              </Styled.CompanyInfo>
            </MatchMedia>
            <Styled.ContactDetails>
              {
                footerContent.wpPage.footerContent.contactDetails.map((contact, index) => {
                  return (
                    <Styled.ContactBlock key={index}>
                      <Heading text={contact.title} />
                      <RawHtmlWrapper content={contact.content} />
                    </Styled.ContactBlock>
                  )
                })
              }
            </Styled.ContactDetails>
          </Styled.Contact>
          <Styled.Socials>
            <MatchMedia breakpoint='md' andAbove>
              <If condition={footerContent.wpPage.footerContent.privacyPolicy}>
                <Link href={footerContent.wpPage.footerContent.privacyPolicy.localFile.publicURL} text='Privacy Policy' />
              </If>
            </MatchMedia>
            <Styled.SocialLinks>
              {
                footerContent.wpPage.footerContent.socials.map((social, index) => {
                  return (
                    <Styled.SocialLink key={index}>
                      <Link href={social.url} text={social.title} />
                    </Styled.SocialLink>
                  )
                })
              }
            </Styled.SocialLinks>
            <MatchMedia breakpoint='sm'>
              <RawHtmlWrapper content={`<p>Social Buff Limited.<br />Company No: ${footerContent.wpPage.footerContent.companyNumber}  |  VAT No: ${footerContent.wpPage.footerContent.vatNumber}</p>`} />
              <If condition={footerContent.wpPage.footerContent.privacyPolicy}>
                <Link href={footerContent.wpPage.footerContent.privacyPolicy.localFile.publicURL} text='Privacy Policy' />
              </If>
            </MatchMedia>
          </Styled.Socials>
        </Container>
      </Styled.Footer>
    </>
  )
}

export default Footer
