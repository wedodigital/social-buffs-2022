import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFooterProps } from './Footer.style.types'

export const Footer = styled.footer((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[6]}px 0 ${props.theme.spacing.fixed[11]}px;
  color: ${props.theme.colours.grey};
  font-size: ${props.theme.typography.paragraph[1].fontSizeMobile};
  line-height: ${props.theme.typography.paragraph[1].lineHeightMobile};

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[11]}px 0;
    font-size: ${props.theme.typography.paragraph[1].fontSize};
    line-height: ${props.theme.typography.paragraph[1].lineHeight};
  `)}

  a {
    text-decoration: none;
  }
`)

export const Contact = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  padding-bottom: ${props.theme.spacing.fixed[6]}px;
  border-bottom: 2px solid ${props.theme.colours.gold};
  display: flex;
  justify-content: space-between;
  align-items: flex-end;

  ${props.theme.mixins.respondTo.md(css`
    padding-bottom: ${props.theme.spacing.fixed[9]}px;
  `)}
`)

export const CompanyInfo = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  svg {
    margin-bottom: ${props.theme.spacing.fixed[2]}px;
  }
`)

export const ContactDetails = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-direction: column;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
    justify-content: flex-end;
  `)}

`)

export const ContactBlock = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[4]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin: 0 ${props.theme.spacing.fixed[10]}px 0 0;
  `)}

  &:last-child {
    margin: 0;
  }

  a {
    color: ${props.theme.colours.grey};
  }
`)

export const Socials = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  padding-top: ${props.theme.spacing.fixed[6]}px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  font-size: ${props.theme.typography.paragraph[2].fontSizeMobile};
  line-height: ${props.theme.typography.paragraph[2].lineHeightMobile};

  & > * {
    margin-bottom: ${props.theme.spacing.fixed[4]}px;

    &:last-child {
      margin-bottom: 0;
    }
  }

  ${props.theme.mixins.respondTo.md(css`
    padding-top: ${props.theme.spacing.fixed[7]}px;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${props.theme.typography.paragraph[2].fontSize};
    line-height: ${props.theme.typography.paragraph[2].lineHeight};
  `)}
`)

export const SocialLinks = styled.ul((): FlattenSimpleInterpolation => css`
  display: flex;
  justify-content: flex-end;
`)

export const SocialLink = styled.li((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  margin-right: ${props.theme.spacing.fixed[8]}px;

  &:last-child {
    margin-right: 0;
  }
`)
