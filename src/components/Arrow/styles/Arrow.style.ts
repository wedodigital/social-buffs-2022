import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledArrowProps } from './Arrow.style.types'

export const Arrow = styled.div((props: StyledArrowProps): FlattenSimpleInterpolation => css`
  width: ${props.theme.spacing.fixed[6]}px;
  height: ${props.theme.spacing.fixed[6]}px;
  background: ${props.theme.colours.black};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 50%;
  z-index: 10;
  cursor: pointer;

  ${props.direction === 'prev' && css`
    left: -${props.theme.spacing.fixed[4]}px;
    transform: translate(50%, -50%);
  `}

  ${props.direction === 'next' && css`
    right: -${props.theme.spacing.fixed[4]}px;
    transform: translate(-50%, -50%);
  `}

  & > svg {
    width: ${props.theme.spacing.fixed[2]}px;
    height: ${props.theme.spacing.fixed[2]}px;

    * {
      fill: ${props.theme.colours.white};
    }
  }
`)
