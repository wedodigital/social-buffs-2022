import React, { ReactElement, FC } from 'react'

import AngleLeft from '@assets/svg/angle-left.svg'
import AngleRight from '@assets/svg/angle-right.svg'

import MatchMedia from '@components/MatchMedia'

import * as Styled from './styles/Arrow.style'

import { ArrowProps } from './Arrow.types'

const Arrow: FC<ArrowProps> = ({
  direction,
  onClick,
}: ArrowProps): ReactElement => {
  if (onClick === undefined) return null
  return (
    <MatchMedia breakpoint='md' andAbove>
      <Styled.Arrow onClick={onClick} direction={direction}>
        <Choose>
          <When condition={direction === 'prev'}>
            <AngleLeft />
          </When>
          <When condition={direction === 'next'}>
            <AngleRight />
          </When>
        </Choose>
      </Styled.Arrow>
    </MatchMedia>
  )
}

export default Arrow
