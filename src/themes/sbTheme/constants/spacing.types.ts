export type SpacingKeys = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11

export type Spacing = {
  unit: number
  fixed: { [P in SpacingKeys]: number }
  responsive: {
    [P in SpacingKeys]: {
      sm: number
      md: number
      lg: number
      xl: number
    }
  }
}
