const typography = {
  paragraph: {
    1: {
      fontSizeMobile: '12px',
      lineHeightMobile: '16px',
      fontSize: '12px',
      lineHeight: '16px',
    },
    2: {
      fontSizeMobile: '14px',
      lineHeightMobile: '18px',
      fontSize: '14px',
      lineHeight: '18px',
    },
    3: {
      fontSizeMobile: '16px',
      lineHeightMobile: '24px',
      fontSize: '16px',
      lineHeight: '24px',
    },
  },
  heading: {
    1: {
      fontSizeMobile: '14px',
      lineHeightMobile: '24px',
      fontSize: '14px',
      lineHeight: '24px',
    },
    2: {
      fontSizeMobile: '18px',
      lineHeightMobile: '24px',
      fontSize: '22px',
      lineHeight: '30px',
    },
    3: {
      fontSizeMobile: '22px',
      lineHeightMobile: '30px',
      fontSize: '32px',
      lineHeight: '48px',
    },
    4: {
      fontSizeMobile: '32px',
      lineHeightMobile: '40px',
      fontSize: '60px',
      lineHeight: '68px',
    },
  },
  fontWeight: {
    1: 100,
    2: 400,
    3: 600,
  },
  fontFamily: {
    body: '"Graphik", Helvetica, Arial, sans-serif',
    feature: '"Nimbus Sans D OT Extended", Helvetica, Arial, sans-serif',
  }
}

export default typography
