import { css, FlattenSimpleInterpolation } from 'styled-components'

import { Theme } from '../sbTheme.types'

export default (fn: (theme: Theme, ...params: unknown[]) => FlattenSimpleInterpolation) => {
  return (...args: unknown[]): FlattenSimpleInterpolation =>
    css`${({ theme }: { theme: Theme }): FlattenSimpleInterpolation => {
      return fn(theme, ...args)
    }}` as FlattenSimpleInterpolation
}
