import React, { FC, ReactElement } from 'react'
import { graphql } from 'gatsby'

import HeadTags from '@components/HeadTags'
import FullPageFeature from '@components/FullPageFeature'
import Section from '@components/Section'
import FeatureImage from '@components/FeatureImage'
import SplitContent from '@components/SplitContent'
import ResultsGrid from '@components/ResultsGrid'
import CaseStudyGrid from '@components/CaseStudyGrid'
import CaseStudyIntro from '@components/CaseStudyIntro'
import MatchMedia from '@components/MatchMedia'
import ImageCarousel from '@components/ImageCarousel'

interface CaseStudyData {
  data: {
    allWpCaseStudy: {
      nodes: {
        seo: unknown
        title: string
        featuredImage: {
          node: {
            sourceUrl: string
          }
        }
        categories: {
          nodes: {
            name: string
          }[]
        }
        tags: {
          nodes: {
            name: string
          }[]
        }
        caseStudyContent: {
          intro: string
          overview: string
          url: string
          mainContent: {
            fieldGroupName: string
            image: {
              sourceUrl: string
            }
            orientation: 'default' | 'reverse'
            content: string
          }[]
          results: {
            percentage: string
            title: string
          }[]
          related: {
            node: {
              title: string
              uri: string
              id: string
              featuredImage: {
                node: {
                  sourceUrl: string
                }
              }
              tags: {
                nodes: {
                  name: string
                }[]
              }
              categories: {
                nodes: {
                  name: string
                }[]
              }
            }[]
          }
        }
      }[]
    }
  }
}

const CaseStudyTemplate: FC<CaseStudyData> = ({ data }: CaseStudyData): ReactElement => {
  const pageData = data.allWpCaseStudy.nodes[0]
  return (
    <>
      <HeadTags seo={pageData.seo} />
      <FullPageFeature
        title={pageData.title}
        backgroundImage={pageData.featuredImage.node.sourceUrl}
        categories={pageData.categories}
      />
      <Section>
        <CaseStudyIntro
          intro={pageData.caseStudyContent.intro}
          overview={pageData.caseStudyContent.overview}
          tags={pageData.tags}
          url={pageData.caseStudyContent.url}
        />
      </Section>
      {
        pageData.caseStudyContent.mainContent.map((section, index) => {
          const sectionPrefix = 'caseStudy_Casestudycontent_MainContent_'
          return (
            <Choose key={index}>
              <When condition={section.fieldGroupName === `${sectionPrefix}FeatureImage`}>
                <MatchMedia breakpoint='md' andAbove>
                  <Section paddingLevel={2}>
                    <FeatureImage image={section.image.sourceUrl} />
                  </Section>
                </MatchMedia>
              </When>
              <When condition={section.fieldGroupName === `${sectionPrefix}SplitContentSection`}>
                <Section paddingLevel={2}>
                  <SplitContent
                    orientation={section.orientation}
                    title={section.title}
                    content={section.content}
                    image={section.image.sourceUrl}
                  />
                </Section>
              </When>
            </Choose>
          )
        })
      }
      <MatchMedia breakpoint='sm'>
        <Section>
          <ImageCarousel>
            {
              pageData.caseStudyContent.mainContent.map((section, index) => {
                if (section.fieldGroupName === 'caseStudy_Casestudycontent_MainContent_FeatureImage') {
                  return (
                    <FeatureImage key={index} image={section.image.sourceUrl} />
                  )
                }
                return null
              })
            }
          </ImageCarousel>
        </Section>
      </MatchMedia>
      <Section>
        <ResultsGrid
          title='Results-Driven, Performance Obsessed.'
          results={pageData.caseStudyContent.results}
        />
      </Section>
      <Section>
        <CaseStudyGrid
          title='Related Work'
          caseStudies={pageData.caseStudyContent.related}
          format='carousel'
        />
      </Section>
    </>
  )
}

export default CaseStudyTemplate

export const pageQuery = graphql`
  query($id: String!) {
    allWpCaseStudy(filter: { id: { eq: $id } }) {
      nodes {
        title
        content
        featuredImage {
          node {
            sourceUrl
          }
        }
        categories {
          nodes {
            name
          }
        }
        tags {
          nodes {
            name
          }
        }
        caseStudyContent {
          intro
          overview
          url
          mainContent {
            ... on WpCaseStudy_Casestudycontent_MainContent_SplitContentSection {
              fieldGroupName
              title
              content
              orientation
              image {
                sourceUrl
              }
            }
            ... on WpCaseStudy_Casestudycontent_MainContent_FeatureImage {
              fieldGroupName
              image {
                sourceUrl
              }
            }
          }
          results {
            percentage
            title
          }
          related {
            node {
              ... on WpCaseStudy {
                title
                uri
                id
                featuredImage {
                  node {
                    sourceUrl
                  }
                }
                tags {
                  nodes {
                    name
                  }
                }
                categories {
                  nodes {
                    name
                  }
                }
              }
            }
          }
        }
        seo {
          metaDesc
          metaKeywords
          canonical
          opengraphType
          opengraphUrl
          opengraphTitle
          opengraphSiteName
          opengraphPublisher
          opengraphPublishedTime
          opengraphModifiedTime
          opengraphImage {
            sourceUrl
          }
          twitterTitle
          twitterDescription
          title
          twitterImage {
            sourceUrl
          }
          schema {
            articleType
            pageType
            raw
          }
        }
      }
    }
  }
`
