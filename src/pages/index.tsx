import React, { FC, ReactElement } from 'react'
import { graphql } from 'gatsby'

import HeadTags from '@components/HeadTags'
import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'
import Section from '@components/Section'
import FullPageFeature from '@components/FullPageFeature'
import LogoGrid from '@components/LogoGrid'
import LargeTextSection from '@components/LargeTextSection'
import CaseStudyGrid from '@components/CaseStudyGrid'
import ServiceGrid from '@components/ServiceGrid'
import TeamSlider from '@components/TeamSlider'
import PartnershipGrid from '@components/PartnershipGrid'
import Headquarters from '@components/Headquarters'

interface HomepageData {
  data: {
    allWpCaseStudy: {
      edges: unknown
    }
    wpPage: {
      seo: unknown
      homepageContent: {
        featureTitle: string
        featureTitleHighlight: string
        featureTitleSuffix: string
        mp4: {
          mediaItemUrl: string
        }
        webm: {
          mediaItemUrl: string
        }
        poster: {
          sourceUrl: string
        }
        logos: {
          logo: {
            sourceUrl: string
          }[]
        }
        introContent: string
        servicesTitle: string
        servicesTitleHighlight: string
        servicesTitleSuffix: string
        services: {
          title: string
          content: string
        }[]
        servicesFootnote: string
        theTeam: {
          name: string
          role: string
          picture: {
            sourceUrl: string
          }
        }[]
        partnershipsTitle: string
        partnerships: {
          companyLogo: {
            sourceUrl: string
          }
        }[]
        hqTitle: string
        hqContent: string
        address: string
        email: string
        telephoneNumber: string
        hqImage: {
          sourceUrl: string
        }
      }
    }
  }
}

const IndexPage: FC<HomepageData> = ({ data }: HomepageData): ReactElement => {
  const pageData = data.wpPage.homepageContent
  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <FullPageFeature
        title={pageData.featureTitle}
        titleHighlight={pageData.featureTitleHighlight}
        titleSuffix={pageData.featureTitleSuffix}
        backgroundVideo={{
          poster: pageData.poster.sourceUrl,
          mp4: pageData.mp4.mediaItemUrl,
          webm: pageData.webm.mediaItemUrl,
        }}
      />
      <Section>
        <LogoGrid
          logos={pageData.logos}
        />
      </Section>
      <Section>
        <LargeTextSection content={pageData.introContent} />
      </Section>
      <Section>
        <CaseStudyGrid
          title='Featured Work'
          showLink
          caseStudies={data.allWpCaseStudy.edges}
        />
      </Section>
      <Section>
        <Heading
          size={4}
          text={pageData.servicesTitle}
          highlightText={pageData.servicesTitleHighlight}
          suffixText={pageData.servicesTitleSuffix}
        />
      </Section>
      <Section>
        <ServiceGrid
          services={pageData.services}
        />
      </Section>
      <If condition={pageData.servicesFootnote}>
        <Section>
          <Paragraph text={pageData.servicesFootnote} />
        </Section>
      </If>
      <If condition={pageData.theTeam}>
        <Section>
          <TeamSlider
            title='Meet The Team'
            team={pageData.theTeam}
          />
        </Section>
      </If>
      <Section>
        <PartnershipGrid
          title={pageData.partnershipsTitle}
          partners={pageData.partnerships}
        />
      </Section>
      <Section>
        <Headquarters
          title={pageData.hqTitle}
          content={pageData.hqContent}
          address={pageData.address}
          email={pageData.email}
          telephoneNumber={pageData.telephoneNumber}
          image={pageData.hqImage.sourceUrl}
        />
      </Section>
    </>
  )
}

export default IndexPage

export const homepageQuery = graphql`
  query homePage {
    allWpCaseStudy(limit: 6) {
      edges {
        node {
          title
          uri
          id
          featuredImage {
            node {
              sourceUrl
            }
          }
          categories {
            nodes {
              name
            }
          }
          tags {
            nodes {
              name
            }
          }
        }
      }
    }
    wpPage(databaseId: {eq: 375}) {
      homepageContent {
        featureTitle
        featureTitleHighlight
        featureTitleSuffix
        mp4 {
          mediaItemUrl
        }
        webm {
          mediaItemUrl
        }
        poster {
          sourceUrl
        }
        logos {
          logo {
            sourceUrl
          }
        }
        introContent
        servicesTitle
        servicesTitleHighlight
        servicesTitleSuffix
        services {
          title
          content
        }
        servicesFootnote
        theTeam {
          name
          role
          picture {
            sourceUrl
          }
        }
        partnershipsTitle
        partnerships {
          companyLogo {
            sourceUrl
          }
        }
        hqTitle
        hqContent
        address
        email
        telephoneNumber
        hqImage {
          sourceUrl
        }
      }
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
    }
  }
`
