import React, { FC, ReactElement } from 'react'
import { graphql } from 'gatsby'

import CaseStudyGrid from '@components/CaseStudyGrid'
import Section from '@components/Section'
import Heading from '@components/Heading'

interface CaseStudyData {
  data: {
    allWpCaseStudy: {
      edges: unknown
    }
    wpPage: {
      title: string
      caseStudyOverview: {
        subtitle: string
      }
    }
  }
}

const CaseStudiesPage: FC<CaseStudyData> = ({ data }: CaseStudyData): ReactElement => {
  const allCaseStudies = data.allWpCaseStudy.edges
  return (
    <>
      <Section titleSection>
        <Heading
          level={1}
          size={4}
          highlightText={data.wpPage.title}
          text={data.wpPage.caseStudyOverview.subtitle}
        />
      </Section>
      <Section>
        <CaseStudyGrid
          caseStudies={allCaseStudies}
        />
      </Section>
    </>
  )
}

export default CaseStudiesPage

export const csQuery = graphql`
  query caseStudyPage {
    allWpCaseStudy {
      edges {
        node {
          title
          uri
          id
          featuredImage {
            node {
              sourceUrl
            }
          }
          tags {
            nodes {
              name
            }
          }
        }
      }
    }
    wpPage(databaseId: {eq: 378}) {
      title
      caseStudyOverview {
        subtitle
      }
    }
  }
`
