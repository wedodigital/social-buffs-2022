import React from 'react'
import PageLayout from './src/templates/PageLayout'

export const wrapPageElement = ({ element }) => <PageLayout>{element}</PageLayout>
