declare let If: React.SFC<{ condition: any }>
declare let For: React.SFC<{ each: string; index: string; of: any[] }>
declare let Choose: React.SFC
declare let When: React.SFC<{ condition: any }>
declare let Otherwise: React.SFC
